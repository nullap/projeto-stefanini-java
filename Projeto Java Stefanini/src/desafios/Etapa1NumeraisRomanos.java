package desafios;

public class Etapa1NumeraisRomanos {

	public Etapa1NumeraisRomanos() {
		// TODO Auto-generated constructor stub
	}

	public String conversorIndoArabicoParaRomano(int numero) {
		int[] vaNum = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};

        String[] vaRom = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};

        while (numero > 0 && numero < 4000) {
            
            if (numero == 0) break;           
            int i = 0;
            while (numero > 0) {
                if (numero >= vaNum[i]) {
                    System.out.print(vaRom[i]);
                    numero -= vaNum[i];
                } else {
                    i++;
                }
            }
            System.out.println();            
        }
        return "";
	}
	
	//public String 
	
}
