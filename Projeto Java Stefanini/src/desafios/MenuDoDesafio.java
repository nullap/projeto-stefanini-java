package desafios;

import java.util.Scanner;

public class MenuDoDesafio {
	static Scanner scanner = new Scanner(System.in);
	
	public static void menuInicial() {
		System.out.println("\t\n\nDESAFIO STEFANI");
		System.out.println("Digite o numero correspondente\nda sua opcao para fazer uma escolha.");
		System.out.println("1 - ir para etapa 1: Numerais Romanos");
		System.out.println("2 - ir para etapa 2: String Balanceada");
		System.out.println("3 - ir para etapa 3: Celulas Visitadas");
		System.out.println("4 - Sair do Programa");
	}
	
	public static void escolhaEtapa1() throws InterruptedException {
		System.out.println("Voc� esta visualizando a etapa 1");
		System.out.println("Caso queira voltar no Menu, digite 0");
		Etapa1NumeraisRomanos e1 = new Etapa1NumeraisRomanos();
		System.out.println("Digite um numero no intevalo de 1 a 3999");
		int numero = scanner.nextInt();
		if(numero == 0) {
			System.out.println("Voltando para o Menu, aguarde...");	
			Thread.sleep(2000);
		} else if(numero > -1 || numero < 4000) {
		System.out.println("Numero convertido com sucesso!\nSeu resultado e:" );
		e1.conversorIndoArabicoParaRomano(numero);
		System.out.println("Voltando ao Menu, aguarde...");
		Thread.sleep(2000);
		} else {
			System.out.println("Numero fora intervalo!\nPor Favor, repita o processo.");
			System.out.println("Voltando ao Menu, aguarde...");
			Thread.sleep(2000);
		}
	}
	public static void escolhaEtapa2() throws InterruptedException {
		System.out.println("Voc� esta visualizando a etapa 2");
		System.out.println("Caso queira voltar no Menu, digite #");
		Etapa2StringBalanceada e2 = new Etapa2StringBalanceada();
		System.out.println("Digite uma String para verificar seu balanceamento");
		String entrada = scanner.nextLine();		
		boolean resposta = e2.verificadorBalanceamentoDeString(entrada);
		if (entrada == "#") {
			System.out.println("Voltando para o Menu, aguarde...");	
			Thread.sleep(2000);
		} else if (resposta) { 
			System.out.println("A String esta balanceada!");
			System.out.println("Voltando ao Menu, aguarde...");
			Thread.sleep(2000);
		} else { 
			System.out.println("A String nao esta balanceada!");
			System.out.println("Voltando ao Menu, aguarde...");
			Thread.sleep(2000);
		}
	}
	public static void escolhaEtapa3() throws InterruptedException {
		System.out.println("Voc� esta visualizando a etapa 3");
		System.out.println("Caso queira voltar no Menu, digite -1");
		Etapa3CelulasVisitadas e3 = new Etapa3CelulasVisitadas();
		System.out.println("Digite o numero de linhas da sua matriz [numero maximo: 100] ");
		int n = scanner.nextInt();
		if(n < 0 || n > 100) {
			System.out.println("numero fora do intervalo permitido, repita o processo");
			Thread.sleep(1000);
		}else {
		System.out.println("Digite o numero de colunas da sua matriz [numero maximo: 100] ");
		int m = scanner.nextInt();
		if(m < 0 || m > 100) {
			System.out.println("numero fora do intervalo permitido, repita o processo");
			Thread.sleep(1000);
		}else {
		System.out.println("Marcos deu "+e3.celulasVisitadas(n,m)+" passos pela matriz");		
		System.out.println("Voltando ao Menu, aguarde...");
		Thread.sleep(3000);
	}
		}
			}
	
	public static void escolhaSair() {
		System.out.println("Voc� Saiu do Programa");
	}

}
