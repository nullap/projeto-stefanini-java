package desafios;

import java.util.Stack;

public class Etapa2StringBalanceada {

	public Etapa2StringBalanceada() {
		// TODO Auto-generated constructor stub
	}

	public boolean verificadorBalanceamentoDeString(String x) {
		 if ((x.length() % 2) == 1) return false;
		  else {
		    Stack<Character> s = new Stack<>();
		    for (char bracket : x.toCharArray())
		      switch (bracket) {
		        case '{': s.push('}'); break;
		        case '(': s.push(')'); break;
		        case '[': s.push(']'); break;
		        default :
		          if (s.isEmpty() || bracket != s.peek()) 
		          { 
		        	  return false;
		          }
		          s.pop();
		      }
		    return s.isEmpty();
		  }
	}
}
